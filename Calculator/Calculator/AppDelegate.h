//
//  AppDelegate.h
//  Calculator
//
//  Created by Irene on 5/30/16.
//  Copyright © 2016 Irene. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

