//
//  ViewController.m
//  Calculator
//
//  Created by Irene on 5/30/16.
//  Copyright © 2016 Irene. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//DISABLE TEXTFIELD EDITING
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    return NO;
}

//SET NUMBER
- (IBAction)buttonTapped:(id)sender {
    switch ([sender tag]){
        case 1:
            self.inputField.text = [NSString stringWithFormat:@"%@1", self.inputField.text];
            break;
        case 2:
            self.inputField.text = [NSString stringWithFormat:@"%@2", self.inputField.text];
            break;
        case 3:
            self.inputField.text = [NSString stringWithFormat:@"%@3", self.inputField.text];
            break;
        case 4:
            self.inputField.text = [NSString stringWithFormat:@"%@4", self.inputField.text];
            break;
        case 5:
            self.inputField.text = [NSString stringWithFormat:@"%@5", self.inputField.text];
            break;
        case 6:
            self.inputField.text = [NSString stringWithFormat:@"%@6", self.inputField.text];
            break;
        case 7:
            self.inputField.text = [NSString stringWithFormat:@"%@7", self.inputField.text];
            break;
        case 8:
            self.inputField.text = [NSString stringWithFormat:@"%@8", self.inputField.text];
            break;
        case 9:
            self.inputField.text = [NSString stringWithFormat:@"%@9", self.inputField.text];
            break;
        case 10:
            self.inputField.text = [NSString stringWithFormat:@"%@0", self.inputField.text];
            break;
        case 16:
            if (![self.inputField.text containsString:@"."]){
                self.inputField.text = [NSString stringWithFormat:@"%@.", self.inputField.text];
            }
            break;
    }
}

//DEFINE OPERATION
- (IBAction)operationButtonTapped:(id)sender {
    switch ([sender tag]){
        case 11:
            operation = ADD;
            break;
        case 12:
            operation = SUBTRACT;
            break;
        case 13:
            operation = MULTIPLY;
            break;
        case 14:
            operation = DIVIDE;
            break;
    }
    value = self.inputField.text;
    self.inputField.text = @"";
}

//CLEAR
- (IBAction)clearButtonTapped:(id)sender {
    self.inputField.text = @"";
}

//CALCULATE RESULT
- (IBAction)equalsButtonTapped:(id)sender {
    NSString * nextNumber = self.inputField.text;
    switch (operation){
        case ADD:
            self.inputField.text = [NSString stringWithFormat:@"%f", [value doubleValue] + [nextNumber doubleValue]];
            break;
        case SUBTRACT:
            self.inputField.text = [NSString stringWithFormat:@"%f", [value doubleValue] - [nextNumber doubleValue]];
            break;
        case MULTIPLY:
            self.inputField.text = [NSString stringWithFormat:@"%f", [value doubleValue] * [nextNumber doubleValue]];
            break;
        case DIVIDE:
            if (![nextNumber isEqualToString:@"0"]){
                self.inputField.text = [NSString stringWithFormat:@"%f", [value doubleValue] / [nextNumber doubleValue]];
            }
            else{
                self.inputField.text = [NSString stringWithFormat:@"Error"];
                value = @"";
            }
            break;
    }
}

//PERCENT BUTTON
- (IBAction)percentButtonTapped:(id)sender {
    value = self.inputField.text;
    self.inputField.text = [NSString stringWithFormat:@"%f", [value doubleValue] / 100];
}

//CHANGE POSITIVE/NEGATIVE
- (IBAction)positiveNegativeButtonTapped:(id)sender {
    if (![self.inputField.text containsString:@"-"]){
        self.inputField.text = [@"-" stringByAppendingString:self.inputField.text];
    }
    else{
        self.inputField.text = [self.inputField.text substringFromIndex:1];
    }
}

@end
