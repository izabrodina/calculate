//
//  ViewController.h
//  Calculator
//
//  Created by Irene on 5/30/16.
//  Copyright © 2016 Irene. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {ADD, SUBTRACT, MULTIPLY, DIVIDE} MathOperation;

@interface ViewController : UIViewController <UITextFieldDelegate> {
    NSString * value;
    MathOperation operation;
}

@property (weak, nonatomic) IBOutlet UITextField *inputField;

@end

